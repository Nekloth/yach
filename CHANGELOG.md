# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Day, month, week and year progressbar
- Hour, minuts and seconds progressbar
- main.css file with customization of CSS
- All JS are in project, as target maybe without internet connection
- ... in fact, everything has been added in this version!
- New clocks (4) in addition to local (considered as PARIS)

## Fixed
- Minor display bug for date (due to existing width value)

## Removed 
- Date, to let some space to clocks

## Changed
- Refactor of the jQuery part to update the time.
- HIde the cursor